<?php

/**
 * @file
 * Administrative part.
 */

/**
 * Settings for H5P SCROM.
 */
function h5p_scorm_admin_settings_form($form, &$form_state) {
  $form = [];

  $form['tracking'] = [
    '#type' => 'fieldset',
    '#title' => t('Tracking'),
  ];

  $form['tracking']['h5p_scorm_leave_completion'] = [
    '#type' => 'checkbox',
    '#title' => t('Leave Complete status'),
    '#description' => t('The Completion status will be leaved to Complete even user restart the course.'),
    '#default_value' => variable_get('h5p_scorm_leave_completion', FALSE),
  ];

  $form['tracking']['h5p_scorm_leave_passed'] = [
    '#type' => 'checkbox',
    '#title' => t('Leave Passed status'),
    '#description' => t('The Passed status will be leaved to Passed even user restart the course.'),
    '#default_value' => variable_get('h5p_scorm_leave_passed', FALSE),
  ];

  $form['tracking']['h5p_scorm_replace_course_score'] = [
    '#type' => 'checkbox',
    '#title' => t('Update score'),
    '#description' => t('The score will be updated each time after test completion. <br/><em>It is possible to set lower score.</em>'),
    '#default_value' => variable_get('h5p_scorm_replace_course_score', FALSE),
  ];

  $form['tracking']['h5p_scorm_passed_status_after_completion'] = [
    '#type' => 'checkbox',
    '#title' => t('Set status to Passed after Completed'),
    '#description' => t('The status will be updated to Passed after course was Complete. <br/><em>It works for packages which are not send the status.</em>'),
    '#default_value' => variable_get('h5p_scorm_passed_status_after_completion', FALSE),
  ];

  $form['tracking']['h5p_scorm_empty_suspend_data'] = [
    '#type' => 'checkbox',
    '#title' => t('Empty the suspend data'),
    '#description' => t('Empty the suspend data on start new attempt. <br/><em>Resume might not work in case the suspend data is empty.</em>'),
    '#default_value' => variable_get('h5p_scorm_empty_suspend_data', FALSE),
  ];

  $form['tracking']['h5p_scorm_bulk_commits'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable bulk commits'),
    '#description' => t('Commits will be sent in bulk.'),
    '#default_value' => variable_get('h5p_scorm_bulk_commits', TRUE),
  ];

  $form['lrs'] = [
    '#type' => 'fieldset',
    '#title' => t('LRS'),
    '#description' => t('LRS settings for TinCan packages'),
  ];

  $form['lrs']['h5p_scorm_lrs_endpoint'] = [
    '#type' => 'textfield',
    '#title' => t('Endpoint'),
    '#description' => t('The server endpoint. Do not include a trailing slash.'),
    '#default_value' => variable_get('h5p_scorm_lrs_endpoint', ''),
  ];

  $form['lrs']['h5p_scorm_lrs_auth_user'] = [
    '#type' => 'textfield',
    '#title' => t('User'),
    '#description' => t('The basic authenication user.'),
    '#default_value' => variable_get('h5p_scorm_lrs_auth_user', ''),
  ];

  $form['lrs']['h5p_scorm_lrs_auth_password'] = [
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('The basic authenication password.'),
    '#default_value' => variable_get('h5p_scorm_lrs_auth_password', ''),
  ];

  return system_settings_form($form);
}
