<?php

/**
 * @file
 * H5P SCORM additional features.
 */

/**
 * Parse imsmanifest.xml file.
 *
 * @param string $file_path
 *   Path to imsmanifest.xml file.
 *
 * @return bool|object
 *   Manifest data.
 */
function h5p_scorm_parse_imsmanifest($file_path) {
  $manifest = FALSE;

  if (!file_exists($file_path)) {
    return $manifest;
  }

  $contents = file_get_contents($file_path);

  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);

  $ok = xml_parse_into_struct($parser, $contents, $values, $tags);
  xml_parser_free($parser);

  if ($ok) {
    $manifest = new StdClass();
    $manifest->identifier = '';
    $manifest->version = '';
    $manifest->schema = '';
    $manifest->schemaversion = '';
    $manifest->root = '';
    $manifest->resource_root = '';
    $manifest->organizations = [];
    $manifest->resources = [];
    $manifest->sequencing = [];

    $default_org_id = '';
    $organization = new stdClass();
    $resource = new stdClass();
    $item = new stdClass();
    $item_stack = [];
    $stack = [];
    $ctag = '';

    // Iterate through values.
    foreach ($values as $e) {
      $objective = new stdClass();

      if ($e['type'] == 'open') {
        $tag = $e['tag'];
        if ($tag == 'manifest') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
          $ctag = $tag;
          $manifest->identifier = $e['attributes']['identifier'] ?? '';
          $manifest->version = $e['attributes']['version'] ?? '';
          $manifest->root = $e['attributes']['xml:base'] ?? '';
        }
        elseif ($tag == 'metadata') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
          $ctag = $tag;
        }
        elseif ($tag == 'organizations') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
          $ctag = $tag;
          $default_org_id = $e['attributes']['default'] ?? '';
        }
        elseif ($tag == 'organization') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
          $ctag = $tag;
          $organization = new stdClass();
          $organization->id = $e['attributes']['identifier'] ?? '';
          $organization->default = FALSE;

          // Set default attribute by organization id or if default not found.
          if ($organization->id == $default_org_id || empty($default_org_id)) {
            $organization->default = TRUE;
          }

          $organization->items = [];
          $organization->controlModeChoice = FALSE;
          $organization->controlModeFlow = FALSE;
        }
        elseif ($tag == 'item') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
          if ($ctag == 'item') {
            array_push($item_stack, $item);
          }
          else {
            $ctag = $tag;
          }
          $item = new stdClass();
          $item->id = $e['attributes']['identifier'] ?? '';
          $item->res_id = $e['attributes']['identifierref'] ?? '';
          $item->parms = $e['attributes']['parameters'] ?? '';
          $item->timelimitaction = '';
          $item->lmsdata = '';
          $item->completionThreshold = FALSE;
          $item->completedByMeasure = FALSE;
          $item->minProgressMeasure = FALSE;
          $item->progressWeight = FALSE;
          $item->items = [];
          $item->objectives = [];
          $item->completionSetByContent = FALSE;
          $item->objectiveSetByContent = FALSE;
        }
        elseif ($tag == 'resources') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
          $manifest->resource_root = $e['attributes']['xml:base'] ?? '';
        }
        elseif ($tag == 'resource') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
          $resource = new stdClass();
          $resource->id = $e['attributes']['identifier'] ?? '';
          $resource->type = $e['attributes']['type'] ?? '';
          $resource->sco_type = $e['attributes']['adicp:scormType'] ?? '';
          $resource->href = $e['attributes']['href'] ?? '';
          $resource->root = $e['attributes']['xml:base'] ?? '';
          $resource->files = [];
          $resource->depends = [];
        }
        elseif ($tag == 'imsss:sequencing') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
        }
        elseif ($tag == 'imsss:objectives') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
        }
        elseif ($tag == 'imsss:primaryObjective') {
          if ($ctag) {
            array_push($stack, $ctag);
          }
          $objective = new stdClass();
          $objective->primary = TRUE;
          $objective->id = $e['attributes']['objectiveID'] ?? '';
          $objective->satisfiedByMeasure = $e['attributes']['satisfiedByMeasure'] ?? FALSE;
        }
      }
      elseif ($e['type'] == 'complete') {
        $tag = $e['tag'];

        if ($tag == 'schema') {
          $manifest->schema = $e['value'] ?? '';
        }
        elseif ($tag == 'schemaversion') {
          $manifest->schemaversion = $e['value'] ?? '';
        }
        elseif ($tag == 'title') {
          if ($ctag == 'organization') {
            $organization->title = $e['value'] ?? '';
          }
          else {
            if ($ctag == 'item') {
              $item->title = $e['value'] ?? '';
            }
          }
        }
        elseif ($tag == 'file') {
          $resource->files[] = $e['attributes']['href'] ?? '';
        }
        elseif ($tag == 'dependency') {
          $resource->depends[] = $e['attributes']['identifierref'] ?? '';
        }
        elseif ($tag == 'adlcp:timeLimitAction') {
          $item->timelimitaction = $e['value'] ?? '';
        }
        elseif ($tag == 'adlcp:dataFromLMS') {
          $item->lmsdata = $e['value'] ?? '';
        }
        elseif ($tag == 'adlcp:completionThreshold') {
          $item->completionThreshold = $e['value'] ?? 0;
          $item->completedByMeasure = $e['attributes']['completedByMeasure'] ?? 0;
          $item->minProgressMeasure = $e['attributes']['minProgressMeasure'] ?? 0;
          $item->progressWeight = $e['attributes']['progressWeight'] ?? 0;
        }
        elseif ($tag == 'imsss:objective') {
          $objective = new stdClass();
          $objective->primary = FALSE;
          $objective->id = $e['attributes']['objectiveID'] ?? '';
          $item->objectives[] = $objective;
        }
        elseif ($tag == 'imsss:minNormalizedMeasure') {
          $objective->minNormalizedMeasure = $e['value'] ?? 0;
        }
        elseif ($tag == 'imsss:deliveryControls' && !empty($item)) {
          $item->completionSetByContent = $e['attributes']['completionSetByContent'] ?? FALSE;
          $item->objectiveSetByContent = $e['attributes']['objectiveSetByContent'] ?? FALSE;
        }
        elseif ($tag == 'imsss:controlMode') {
          $organization->controlModeChoice = $e['attributes']['choice'] ?? FALSE;
          $organization->controlModeFlow = $e['attributes']['flow'] ?? FALSE;
        }
        // This is an empty tag without any tags inside.
        elseif ($tag == 'resource') {
          // Capture resource data.
          $resource = new stdClass();
          $resource->id = $e['attributes']['identifier'] ?? '';
          $resource->type = $e['attributes']['type'] ?? '';
          $resource->sco_type = $e['attributes']['adlcp:scormType'] ?? '';
          $resource->href = $e['attributes']['href'] ?? '';
          $resource->root = $e['attributes']['xml:base'] ?? '';
          $resource->files = [];
          $resource->depends = [];

          // Add resource to manifest resources.
          $manifest->resources[] = $resource;
        }
      }
      // Close the open object and pop the previous open object.
      elseif ($e['type'] == 'close') {

        $tag = $e['tag'];

        if ($tag == 'metadata') {
          $ctag = array_pop($stack);
        }
        elseif ($tag == 'organizations') {
          $ctag = array_pop($stack);
        }
        elseif ($tag == 'organization') {
          $ctag = array_pop($stack);
          $manifest->organizations[] = $organization;
          $organization = new stdClass();
        }
        elseif ($tag == 'item') {
          $ctag = array_pop($stack);
          if ($ctag == 'item') {
            $parent_item = array_pop($item_stack);
            $parent_item->items[] = $item;
            $item = $parent_item;
          }
          else {
            $organization->items[] = $item;
            $item = new stdClass();
          }
        }
        elseif ($tag == 'resources') {
          $ctag = array_pop($stack);
        }
        elseif ($tag == 'resource') {
          $ctag = array_pop($stack);
          $manifest->resources[] = $resource;
          $resource = new stdClass();
        }
        elseif ($tag == 'imsss:sequencing') {
          $ctag = array_pop($stack);
        }
        elseif ($tag == 'imsss:objectives') {
          $ctag = array_pop($stack);
        }
        elseif ($tag == 'imsss:primaryObjective') {
          $ctag = array_pop($stack);
          $item->objectives[] = $objective;
        }
      }
    }
  }

  return $manifest;
}

/**
 * Get organization (default or specific by id).
 *
 * @param object $manifest
 *   Manifest data.
 * @param bool|string $org_id
 *   Organization id or FALSE.
 *
 * @return bool|object
 *   Returns organization data or FALSE.
 */
function h5p_scorm_get_organization($manifest, $org_id = FALSE) {
  $org = FALSE;

  if (empty($manifest)) {
    return $org;
  }

  foreach ($manifest->organizations as $organization) {
    // If no org id specified and org is default, return it.
    if (!$org_id && $organization->default) {
      $org = $organization;
      break;
    }

    // Check for id match.
    if ($org_id && $organization->id == $org_id) {
      $org = $organization;
      break;
    }
  }

  return $org;
}

/**
 * Get SCORM navigation items.
 *
 * @param object $manifest
 *   Manifest data.
 * @param array $org_items
 *   Organization items.
 * @param string $path
 *   URI path to the SCORM package.
 *
 * @return array
 *   Returns navigation list.
 */
function h5p_scorm_nav_list_items($manifest, $org_items, $path) {
  if (empty($manifest)) {
    return [];
  }

  $info = [];

  foreach ($org_items as $org) {
    // Only add items with resources.
    if ($org->res_id) {
      $files = [];

      if (!empty($org->files)) {
        $files = array_merge($files, $org->files);
      }

      $res = h5p_scorm_get_resource($manifest, $org->res_id);

      if (!empty($res->depends)) {
        foreach ($res->depends as $depend) {
          $depend_res = h5p_scorm_get_resource($manifest, $depend);

          if (!empty($depend_res->files)) {
            $files = array_merge($files, $depend_res->files);
          }
        }
      }

      if ($res && $res->href) {
        $uri = $path . DIRECTORY_SEPARATOR . $res->href . $org->parms;
        $uri = file_create_url($uri);

        $info[] = [
          'active' => FALSE,
          'id' => $org->id,
          'lmsdata' => $org->lmsdata,
          'title' => $org->title,
          'url' => urldecode($uri),
          'files' => $files,
        ];
      }
    }

    // Recurse to add inner items.
    if (count($org->items) > 0) {
      $info = array_merge($info, h5p_scorm_nav_list_items($manifest, $org->items, $path));
    }
  }

  return $info;
}

/**
 * Get resource data.
 *
 * @param object $manifest
 *   Manifest data.
 * @param string $res_id
 *   Resource id.
 *
 * @return bool|object
 *   Resource object or FALSE.
 */
function h5p_scorm_get_resource($manifest, $res_id) {
  $res = FALSE;

  if (empty($manifest)) {
    return $res;
  }

  foreach ($manifest->resources as $resource) {
    if ($resource->id == $res_id) {
      $res = $resource;
      break;
    }
  }

  return $res;
}

/**
 * Get the starting resource.
 *
 * @param object $items
 *   Items data.
 *
 * @return bool|object
 *   Returns item object or FALSE.
 */
function h5p_scorm_start_resource($items) {
  if (empty($items)) {
    return FALSE;
  }

  foreach ($items as $item) {
    if (count($item->items) > 0) {
      return h5p_scorm_start_resource($item->items);
    }

    return $item;
  }

  return FALSE;
}

/**
 * Get lrs settings (endpoint, user login and pass).
 */
function h5p_scorm_get_lrs_settings() {
  $settings = [
    'endpoint' => variable_get('h5p_scorm_lrs_endpoint', ''),
    'auth_user' => variable_get('h5p_scorm_lrs_auth_user', ''),
    'auth_password' => variable_get('h5p_scorm_lrs_auth_password', ''),
  ];

  drupal_alter('h5p_scorm_lrs_settings', $settings);

  return $settings;
}

/**
 * Parse tincan.xml file.
 */
function h5p_scorm_parse_tincan($file_path) {
  $tincan = FALSE;

  if (!file_exists($file_path)) {
    return $tincan;
  }

  $contents = file_get_contents($file_path);
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
  $ok = xml_parse_into_struct($parser, $contents, $values, $tags);
  xml_parser_free($parser);

  if ($ok) {
    $tincan = new StdClass();

    $array_tags = ['launch', 'description', 'name', 'activity'];
    foreach ($values as $value) {
      if (in_array($value['tag'], $array_tags) && isset($value['value'])) {
        $tincan->{$value['tag']} = $value['value'];
      }
      elseif (isset($value['attributes']['id'])) {
        $tincan->{$value['tag']} = $value['attributes']['id'];
      }
    }
  }

  return $tincan;
}

/**
 * Generate UUID.
 */
function h5p_scorm_generate_uuid() {
  $random_string = openssl_random_pseudo_bytes(16);
  $time_low = bin2hex(substr($random_string, 0, 4));
  $time_mid = bin2hex(substr($random_string, 4, 2));
  $time_hi_and_version = bin2hex(substr($random_string, 6, 2));
  $clock_seq_hi_and_reserved = bin2hex(substr($random_string, 8, 2));
  $node = bin2hex(substr($random_string, 10, 6));
  $time_hi_and_version = hexdec($time_hi_and_version);
  $time_hi_and_version = $time_hi_and_version >> 4;
  $time_hi_and_version = $time_hi_and_version | 0x4000;
  $clock_seq_hi_and_reserved = hexdec($clock_seq_hi_and_reserved);
  $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved >> 2;
  $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved | 0x8000;

  return sprintf(
    '%08s-%04s-%04x-%04x-%012s',
    $time_low,
    $time_mid,
    $time_hi_and_version,
    $clock_seq_hi_and_reserved,
    $node
  );
}

/**
 * Get TinCan configurations.
 */
function h5p_scorm_get_tincan_configurations() {
  // Get LRS settings.
  $settings = h5p_scorm_get_lrs_settings();

  return [
    'endpoint' => $settings['endpoint'] . '/',
    'auth' => 'Basic ' . base64_encode($settings['auth_user'] . ':' . $settings['auth_password']),
  ];
}

/**
 * Create an 'actor' for the LRS.
 *
 * @param mixed $account
 *   Optional. A Drupal user object or a user id.
 *
 * @return array
 *   An array representing an 'actor'.
 */
function h5p_scorm_get_actor($account = NULL) {
  if (is_null($account)) {
    global $user;
    $account = user_load($user->uid);
  }
  else {
    if (is_int($account)) {
      $account = user_load($account);
    }
  }

  return [
    'name' => [
      format_username($account),
    ],
    'mbox' => [
      $account->mail ? 'mailto:' . $account->mail : 'mailto:' . variable_get('site_mail'),
    ],
    'objectType' => 'Agent',
  ];
}

/**
 * Create the new attempt.
 *
 * @param int $nid
 *   Node ID.
 * @param int $timestamp
 *   Timestamp.
 * @param array $params
 *   Extra parameters.
 *
 * @return int|FALSE
 */
function h5p_scorm_attempt_create($nid, $timestamp = REQUEST_TIME, $params = []) {
  if (empty($nid)) {
    return NULL;
  }

  if (empty($params['uid'])) {
    global $user;

    $params['uid'] = $user->uid;
  }

  $params['nid'] = $nid;

  $query = db_select('h5p_scorm_attempt', 'a');
  $query->fields('a', [
    'uuid',
    'delta',
    'completion_status',
    'success_status',
    'score',
    'cmi_data',
    'activity_objective',
    'page_id',
  ]);
  $query->condition('a.uid', $params['uid']);
  $query->condition('a.nid', $nid);
  $query->addTag('h5p_scorm_attempt_create');
  $query->addMetaData('params', $params);
  $query->orderBy('a.updated', 'DESC');
  $old_data = $query->execute()->fetchAssoc();

  // Prepare objectives list.
  if (
    empty($old_data['cmi_data'])
    && !empty($node = node_load($nid))
    && !empty($node->json_content)
  ) {
    if (empty($old_data)) {
      $old_data = [];
    }

    $objectives = [];
    $json_content = drupal_json_decode($node->json_content);

    if ($manifest = h5p_scorm_parse_imsmanifest($json_content['scorm']['extracted'] . DIRECTORY_SEPARATOR . 'imsmanifest.xml')) {
      // Get default organization.
      $org = h5p_scorm_get_organization($manifest);

      if (!empty($org->items[0]->objectives)) {
        foreach ($org->items[0]->objectives as $objective) {
          if (isset($objective->id)) {
            $objectives[] = [
              'id' => $objective->id,
            ];
          }
        }
      }
    }

    $old_data['cmi_data'] = serialize([
      'objectives' => $objectives,
    ]);
  }

  try {
    $data = [
      'uuid' => $old_data['uuid'] ?? h5p_scorm_generate_uuid(),
      'uid' => $params['uid'],
      'nid' => $nid,
      'delta' => !empty($old_data['delta']) ? $old_data['delta'] + 1 : 1,
      'completion_status' => !empty($old_data['completion_status']) ? $old_data['completion_status'] : '',
      'success_status' => !empty($old_data['success_status']) ? $old_data['success_status'] : '',
      'score' => !empty($old_data['score']) ? $old_data['score'] : NULL,
      'cmi_data' => !empty($old_data['cmi_data']) ? $old_data['cmi_data'] : NULL,
      'activity_report' => NULL,
      'activity_objective' => !empty($old_data['activity_objective']) ? $old_data['activity_objective'] : NULL,
      'page_id' => !empty($old_data['page_id']) ? $old_data['page_id'] : NULL,
      'started' => $timestamp,
      'updated' => $timestamp,
    ];

    $attempt_id =  db_insert('h5p_scorm_attempt')
      ->fields($data)
      ->execute();

    foreach (module_implements('h5p_scorm_attempt_create') as $module) {
      $function = $module . '_h5p_scorm_attempt_create';
      $function($attempt_id, $data, $params);
    }

    return $attempt_id;
  }
  catch (Exception $e) {
    watchdog_exception('h5p_scorm', $e);

    return FALSE;
  }
}

/**
 * Get pages list by node ID.
 *
 * @param int $nid
 *   Node ID.
 *
 * @return array
 *   Package pages list.
 */
function h5p_scorm_get_package_pages($nid) {
  $pages = [];

  if (empty($nid)) {
    return $pages;
  }

  $query = db_select('h5p_nodes', 'n');
  $query->fields('n', ['json_content']);
  $query->condition('n.nid', $nid);
  $query->orderBy('n.content_id', 'DESC');

  if ($data = $query->execute()->fetchField()) {
    if (($data = drupal_json_decode($data)) && isset($data['scorm']['pages'])) {
      foreach ($data['scorm']['pages'] as $page) {
        $pages[$page['id']] = $page['title'];
      }
    }
  }

  return $pages;
}

/**
 * Get page URL.
 *
 * @param int $nid
 *   Node ID.
 * @param string $page_id
 *   Page ID.
 *
 * @return string
 *   Package page URL.
 */
function h5p_scorm_get_package_page_url($nid, $page_id) {
  $url = NULL;

  if (empty($nid)) {
    return $url;
  }

  $query = db_select('h5p_nodes', 'n');
  $query->fields('n', ['json_content']);
  $query->condition('n.nid', $nid);

  if (($data = $query->execute()->fetchField())
    && ($data = drupal_json_decode($data))
    && isset($data['scorm']['pages'])
  ) {
    foreach ($data['scorm']['pages'] as $page) {
      if ($page['id'] == $page_id && !empty($page['url'])) {
        $url = $page['url'];
      }
    }
  }

  return $url;
}

/**
 * Get last attempt.
 *
 * @param int $nid
 *   Node ID.
 * @param int|NULL $uid
 *   User ID.
 * @param array $params
 *   Extra parameters.
 *
 * @return object|FALSE
 */
function h5p_scorm_get_last_attempt($nid, $uid = NULL, $params = []) {
  if (empty($nid)) {
    return FALSE;
  }

  if (empty($uid)) {
    global $user;

    $uid = $user->uid;
  }

  $result = &drupal_static(__FUNCTION__ . '_' . implode('_', [$nid, $uid]));

  if ($result !== NULL) {
    return $result;
  }

  $params['nid'] = $nid;
  $params['uid'] = $uid;

  $query = db_select('h5p_scorm_attempt', 'a');
  $query->fields('a');
  $query->condition('a.nid', $nid);
  $query->condition('a.uid', $uid);
  $query->orderBy('a.aid', 'DESC');
  $query->addTag('h5p_scorm_get_last_attempt');
  $query->addMetaData('params', $params);
  $query->range(0, 1);

  if ($result = $query->execute()->fetchAll()) {
    $result = reset($result);
    $result->cmi_data = unserialize($result->cmi_data);
  }

  return $result;
}

/**
 * Get scorm pages by node ID.
 *
 * @param int $nid
 *   Node ID.
 *
 * @return array
 *   Returns all pages.
 */
function h5p_scorm_pages_by_nid($nid) {
  $pages = &drupal_static(__FUNCTION__ . '_' . $nid);

  if ($pages !== NULL) {
    return $pages;
  }

  $query = db_select('h5p_nodes', 'n');
  $query->fields('n', ['json_content']);
  $query->condition('n.nid', $nid);

  if ($json_content = $query->execute()->fetchField()) {
    $json_content = drupal_json_decode($json_content);

    if (isset($json_content['scorm'], $json_content['scorm']['pages'])) {
      foreach ($json_content['scorm']['pages'] as $page) {
        $pages[$page['id']] = $page['title'];
      }
    }
  }

  return $pages;
}

/**
 * Get init page.
 *
 * @param string $url
 *   Path to extracted package directory.
 *
 * @return string
 *   Path including the init file.
 */
function _h5p_scorm_get_page_init_url($url) {
  if ($manifest = h5p_scorm_parse_imsmanifest($url . DIRECTORY_SEPARATOR . 'imsmanifest.xml')) {
    // Get default organization.
    $org = h5p_scorm_get_organization($manifest);

    // Find the starting resource.
    if ($item = h5p_scorm_start_resource($org->items)) {
      $res = h5p_scorm_get_resource($manifest, $item->res_id);

      if ($res && $res->href) {
        $url .= DIRECTORY_SEPARATOR . $res->href . $item->parms;
      }
    }
  }
  elseif ($tincan = h5p_scorm_parse_tincan($url . DIRECTORY_SEPARATOR . 'tincan.xml')) {
    $url .= DIRECTORY_SEPARATOR . $tincan->launch;
  }

  return base_path() . $url;
}

/**
 * Get list of SCORM pages.
 *
 * @param string $path
 *   Path to extracted package directory.
 *
 * @return array
 *   List of pages.
 */
function _h5p_scorm_get_pages($path) {
  $pages = [];

  if (!$manifest = h5p_scorm_parse_imsmanifest($path . DIRECTORY_SEPARATOR . 'imsmanifest.xml')) {
    return $pages;
  }

  $org = h5p_scorm_get_organization($manifest);

  if ($elements = h5p_scorm_nav_list_items($manifest, $org->items, $path)) {
    foreach ($elements as $element) {
      $pages[] = $element;
    }
  }

  return $pages;
}

/**
 * Copies files from one directory to another.
 *
 * @param string $path_from
 *   Copy files from folder.
 * @param string $path_to
 *   Copy files to folder.
 */
function _h5p_scorm_recursive_copy($path_from, $path_to) {
  if (file_exists($path_to)) {
    @rmdir($path_to);
  }

  if (is_dir($path_from)) {
    @mkdir($path_to);
    $files = scandir($path_from);

    foreach ($files as $file) {
      if ($file == "." || $file == "..") {
        continue;
      }

      _h5p_scorm_recursive_copy("$path_from/$file", "$path_to/$file");
    }
  }
  elseif (file_exists($path_from)) {
    @copy($path_from, $path_to);
  }
}

/**
 * Prepare score value.
 *
 * @param float $value
 *   Score value.
 *
 * @return float
 *   Score formatted value.
 */
function h5p_scorm_score_value($value) {
  return ceil($value * 10) / 10;
}

/**
 * Track attempt.
 *
 * @param object $attempt
 *   Attempt ID.
 * @param array $objective_pages
 *   SCORM objective pages.
 * @param string $page_id
 *   SCORM page ID.
 * @param array $cmi_data
 *   CMI data.
 * @param int $timestamp
 *   Timestamp of the commit.
 * @param string $table
 *   Table name.
 * @param array $params
 *   Extra parameters.
 */
function h5p_scorm_store_attempt_commit($attempt, $objective_pages, $page_id, $cmi_data, $activity_report, $timestamp = REQUEST_TIME, $table = 'h5p_scorm_attempt', $params = []) {
  if (empty($attempt)) {
    return FALSE;
  }

  drupal_alter('h5p_scorm_store_attempt_commit_params', $attempt, $table, $params);

  $old_data = (array) $attempt;

  $new_data = [
    'page_id' => $page_id,
    'activity_report' => !empty($old_data['activity_report']) ? unserialize($old_data['activity_report']) : [],
  ];

  // Prepare activity objective.
  $activity_objective = !empty($old_data['activity_objective']) ? unserialize($old_data['activity_objective']) : [];

  if (!empty($objective_pages)) {
    foreach ($objective_pages as $objective_page_id => $objective_page_title) {
      if (!isset($activity_objective[$objective_page_id])) {
        $activity_objective[$objective_page_id] = [
          'cmi.completion_status' => '',
          'cmi.success_status' => '',
        ];
      }
    }

    // Remove 'objectives' that are not in the scorm object.
    if (!empty($activity_objective)) {
      foreach ($activity_objective as $objective_page_id => $item) {
        if (!isset($objective_pages[$objective_page_id])) {
          unset($activity_objective[$objective_page_id]);
        }
      }
    }
  }

  // Build activity report.
  if (!empty($page_id)) {
    $replace_score = variable_get('h5p_scorm_replace_course_score', FALSE);

    // Build activity objective.
    foreach ($activity_report as $item) {
      // Support SCORM 1.2 statuses.
      if ($item['property'] == 'cmi.core.lesson_status') {
        if (in_array($item['value'], [
          H5P_SCORM_COMPLETION_STATUS_COMPLETED,
          H5P_SCORM_COMPLETION_STATUS_INCOMPLETE,
        ])) {
          $activity_objective[$page_id]['cmi.completion_status'] = $item['value'];
        }

        if (in_array($item['value'], [
          H5P_SCORM_SUCCESS_STATUS_PASSED,
          H5P_SCORM_SUCCESS_STATUS_FAILED,
        ])) {
          $activity_objective[$page_id]['cmi.success_status'] = $item['value'];
        }
      }
      elseif ($item['property'] == 'cmi.completion_status') {
        if (
          empty($activity_objective[$page_id][$item['property']])
          || $activity_objective[$page_id][$item['property']] != H5P_SCORM_COMPLETION_STATUS_COMPLETED
          || empty(variable_get('h5p_scorm_leave_completion', FALSE))
        ) {
          $activity_objective[$page_id][$item['property']] = $item['value'];
        }
      }
      elseif ($item['property'] == 'cmi.success_status') {
        if (
          empty($activity_objective[$page_id][$item['property']])
          || $activity_objective[$page_id][$item['property']] != H5P_SCORM_SUCCESS_STATUS_PASSED
          || empty(variable_get('h5p_scorm_leave_passed', FALSE))
        ) {
          $activity_objective[$page_id][$item['property']] = $item['value'];
        }
      }
      elseif (in_array($item['property'], [
        'cmi.score.min',
        'cmi.score.max',
        'cmi.core.score.min',
        'cmi.core.score.max',
      ])) {
        $activity_objective[$page_id][$item['property']] = $item['value'];
      }
      elseif (in_array($item['property'], [
        'cmi.score.raw',
        'cmi.score.scaled',
        'cmi.core.score.raw',
        'cmi.core.score.scaled',
      ])) {
        if (!isset($activity_objective[$page_id][$item['property']])
          || $replace_score
          || $activity_objective[$page_id][$item['property']] < $item['value']
        ) {
          $activity_objective[$page_id][$item['property']] = $item['value'];

          if (in_array($item['property'], [
            'cmi.score.raw',
            'cmi.core.score.raw',
          ])) {
            module_invoke_all('h5p_scorm_commit_score', $attempt->nid, $item['value'], $params, $table);
          }
        }
      }
    }

    if (!isset($activity_objective[$page_id]['cmi.completion_status'])
      || empty($activity_objective[$page_id]['cmi.completion_status'])
    ) {
      $activity_objective[$page_id]['cmi.completion_status'] = H5P_SCORM_COMPLETION_STATUS_INCOMPLETE;
      $activity_report[] = [
        'property' => 'cmi.completion_status',
        'value' => H5P_SCORM_COMPLETION_STATUS_INCOMPLETE,
      ];
    }
  }

  $new_data['activity_report'][] = [
    $timestamp => $activity_report,
  ];

  $completion = [
    'completed' => 0,
    'incomplete' => 0,
  ];

  $success = [
    'passed' => 0,
    'failed' => 0,
  ];

  $scores = 0;
  $quizzes = 0;

  // Calculate values.
  foreach ($activity_objective as $objective) {
    if (empty($objective)) {
      continue;
    }

    if (isset($objective['cmi.completion_status'])) {
      if ($objective['cmi.completion_status'] == H5P_SCORM_COMPLETION_STATUS_COMPLETED) {
        $completion['completed']++;
      }
      elseif ($objective['cmi.completion_status'] == H5P_SCORM_COMPLETION_STATUS_INCOMPLETE) {
        $completion['incomplete']++;
      }
    }

    if (isset($objective['cmi.success_status'])) {
      if ($objective['cmi.success_status'] == H5P_SCORM_SUCCESS_STATUS_PASSED) {
        $success['passed']++;
      }
      elseif ($objective['cmi.success_status'] == H5P_SCORM_SUCCESS_STATUS_FAILED) {
        $success['failed']++;
      }
    }

    if (!empty($objective['cmi.score.scaled']) && $objective['cmi.score.scaled'] <= 1) {
      $scores += round($objective['cmi.score.scaled'] * 100);
      $quizzes++;
    }
    elseif (isset($objective['cmi.score.raw'])) {
      $scores += (float) $objective['cmi.score.raw'];
      $quizzes++;
    }

    if (!empty($objective['cmi.core.score.scaled']) && $objective['cmi.core.score.scaled'] <= 1) {
      $scores += round($objective['cmi.core.score.scaled'] * 100);
      $quizzes++;
    }
    elseif (isset($objective['cmi.core.score.raw'])) {
      $scores += (float) $objective['cmi.core.score.raw'];
      $quizzes++;
    }
  }

  if ($completion['completed'] == count($activity_objective)) {
    $new_data['completion_status'] = H5P_SCORM_COMPLETION_STATUS_COMPLETED;

    if (variable_get('h5p_scorm_passed_status_after_completion', FALSE)) {
      $new_data['success_status'] = H5P_SCORM_SUCCESS_STATUS_PASSED;
    }
  }
  else {
    $new_data['completion_status'] = H5P_SCORM_COMPLETION_STATUS_INCOMPLETE;
  }

  if ($success['failed'] > 0) {
    $new_data['success_status'] = H5P_SCORM_SUCCESS_STATUS_FAILED;
  }
  elseif ($success['passed'] == count($activity_objective)) {
    $new_data['success_status'] = H5P_SCORM_SUCCESS_STATUS_PASSED;
    // Set complete for passed courses.
    $new_data['completion_status'] = H5P_SCORM_COMPLETION_STATUS_COMPLETED;

    if (!empty($page_id)) {
      $activity_objective[$page_id]['cmi.completion_status'] = H5P_SCORM_COMPLETION_STATUS_COMPLETED;
    }
  }

  if ($quizzes > 0) {
    $new_data['score'] = h5p_scorm_score_value($scores / $quizzes);
  }

  if (
    isset($new_data['completion_status'])
    && isset($old_data['completion_status'])
  ) {
    // Custom logs.
    if ($new_data['completion_status'] != $old_data['completion_status']) {
      $new_data['activity_report'][] = [
        $timestamp => [
          [
            'property' => 'completion_status',
            'value' => $new_data['completion_status'],
          ],
        ],
      ];
    }

    if (
      $old_data['completion_status'] == H5P_SCORM_COMPLETION_STATUS_COMPLETED
      && !empty(variable_get('h5p_scorm_leave_completion', FALSE))
    ) {
      $new_data['completion_status'] = $old_data['completion_status'];
    }
  }

  if (
    isset($new_data['success_status'])
    && isset($old_data['success_status'])
    && $new_data['success_status'] != $old_data['success_status']
  ) {
    // Custom logs.
    $new_data['activity_report'][] = [
      $timestamp => [
        [
          'property' => 'success_status',
          'value' => $new_data['success_status'],
        ],
      ],
    ];

    if (
      $old_data['success_status'] == H5P_SCORM_SUCCESS_STATUS_PASSED
      && !empty(variable_get('h5p_scorm_leave_passed', FALSE))
    ) {
      $new_data['success_status'] = $old_data['success_status'];
    }
  }

  if ($old_data['started'] > $timestamp) {
    $old_data['started'] = $timestamp;
  }

  $new_data['total_time'] = $timestamp - $old_data['started'];
  $new_data['activity_report'] = serialize($new_data['activity_report']);
  $new_data['activity_objective'] = serialize($activity_objective);
  $new_data['cmi_data'] = serialize($cmi_data);
  $new_data['updated'] = $timestamp;

  try {
    db_merge($table)
      ->key([
        'aid' => $attempt->aid,
      ])
      ->fields($new_data)
      ->execute();

    $new_attempt = (object) $new_data;

    foreach (module_implements('h5p_scorm_store_attempt_commit') as $module) {
      $function = $module . '_h5p_scorm_store_attempt_commit';
      $function($attempt, $new_attempt, $params, $table);
    }

    return $new_attempt;
  }
  catch (Exception $e) {
    watchdog_exception('h5p_scorm', $e);

    return FALSE;
  }
}

/**
 * Format CMI data according to scorm-again.js.
 *
 * @param array $cmi_data
 *   CMI data.
 * @param string $version
 *   SCORM version.
 *
 * @return object
 */
function h5p_scorm_cmi_data_format($cmi_data, $version) {
  if ($version == '1.2'
    && !empty($cmi_data['suspend_data'])
    && drupal_strlen($cmi_data['suspend_data']) > 4096
  ) {
    unset($cmi_data['suspend_data']);
  }

  // Remove interactions without required field - id.
  if ($version == '2004' && !empty($cmi_data['interactions'])) {
    $interactions = [];

    foreach ($cmi_data['interactions'] as $interaction) {
      if (isset($interaction['id']) && !empty($interaction['id'])) {
        continue;
      }

      $interactions[] = $interaction;
    }

    if (!empty($interactions)) {
      $cmi_data['interactions'] = $interactions;
    }
  }

  foreach ($cmi_data as $attribute => $value) {
    if (is_numeric($value)) {
      if ($attribute == 'latency') {
        $cmi_data[$attribute] = sprintf('%02d:%02d:%02d', ($value / 3600), ($value / 60 % 60), $value % 60);
      }
      else {
        $cmi_data[$attribute] = (string) $value;
      }
    }

    if (is_array($value)) {
      $cmi_data[$attribute] = h5p_scorm_cmi_data_format($value, $version);
    }
  }

  return (object) $cmi_data;
}
