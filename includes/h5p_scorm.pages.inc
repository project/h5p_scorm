<?php

/**
 * @file
 * H5P SCORM menu features.
 */

/**
 * Callback for SCORM configuration.
 */
function h5p_scorm_config_callback($token) {
  if (!H5PCore::validToken('editorajax', $token)) {
    H5PCore::ajaxError(t('Invalid security token. Please reload the editor.'));
    drupal_exit();
  }

  $upload_max_size = file_upload_max_size();

  drupal_json_output(array(
    'upload_max_size' => array(
      'raw' => $upload_max_size,
      'value' => format_size($upload_max_size),
    ),
  ));

  drupal_exit();
}

/**
 * Callback for file uploads.
 */
function h5p_scorm_file_callback($token) {
  if (!H5PCore::validToken('editorajax', $token)) {
    H5PCore::ajaxError(t('Invalid security token. Please reload the editor.'));
    drupal_exit();
  }

  $files_directory = file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath() . '/h5peditor';
  $file = new H5pScormFile(_h5p_get_instance('interface'), $files_directory);

  if (!$file->isLoaded()) {
    H5PCore::ajaxError(t('File is not loaded.'));
    drupal_exit();
  }

  if ($file->validate()) {
    $file->copy();
  }

  header('Content-type: text/html; charset=utf-8');
  print $file->getResult();
  drupal_exit();
}

/**
 * Empty callback to process SCORM commit.
 */
function h5p_scorm_commit_callback() {
  return [
    'result' => TRUE,
  ];
}

/**
 * Callback to process SCORM commit.
 *
 * @param object $attempt
 *   Attempt object.
 *
 * @return array
 */
function h5p_scorm_commit_attempt_callback($attempt) {
  $response = [
    'result' => FALSE,
  ];

  $commit = $_POST;

  if (!empty($commit['page_id'])
    && !empty($commit['cmi'])
    && !empty($commit['timestamp'])
  ) {
    $objective_pages = h5p_scorm_get_package_pages($attempt->nid);
    $new_attempt = h5p_scorm_store_attempt_commit($attempt, $objective_pages, $commit['page_id'], $commit['cmi'], $commit['activity_report'] ?? [], $commit['timestamp']);
    $response['result'] = TRUE;

    drupal_alter('h5p_scorm_commit_response', $response, $attempt, $new_attempt);
  }

  return $response;
}
